package pcd.ass02.es3;

import java.io.File;
import io.reactivex.rxjava3.core.*;

public class DocStreamBuilder {
	private File startDir;
	private Flag stopFlag;
	
	public DocStreamBuilder(File dir, Flag stopFlag) {
		this.startDir = dir;	
		this.stopFlag = stopFlag;
	}
	
	public Observable<File> createDocStream(){		
	    return Observable.create(emitter -> {
			explore(startDir, emitter);
			emitter.onComplete();
	    });
	}
		
	private void explore(File dir, ObservableEmitter<File> emitter) {
		if (!stopFlag.isSet()) {
			for (File f: dir.listFiles()) {
				if (f.isDirectory()) {
					explore(f, emitter);
				} else if (f.getName().endsWith(".pdf")) {
					try {
						logDebug("find a new doc: " + f.getName());
						emitter.onNext(f);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} 
	}

	protected void log(String msg) {
		System.out.println("[ Doc Discover Stream ] " + msg);
	}

	protected void logDebug(String msg) {
		System.out.println("[ Doc Discover Stream ] " + msg);
	}

}
