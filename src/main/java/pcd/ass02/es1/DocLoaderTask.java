package pcd.ass02.es1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

public class DocLoaderTask implements Callable<List<Future<Void>>> {

	private PDFTextStripper stripper;
	private File doc;
	private SharedData sharedData;

	public DocLoaderTask(File doc, SharedData sharedData) throws Exception {
		this.doc = doc;
		this.sharedData = sharedData;
		stripper = new PDFTextStripper();
	}

	public List<Future<Void>> call() {
		logDebug(" started - working on " + doc.getName());
		List<Future<Void>> futs = new ArrayList<Future<Void>>();
		try {
			PDDocument document = PDDocument.load(doc);
			AccessPermission ap = document.getCurrentAccessPermission();
			if (ap.canExtractContent()) {
				int nPages = document.getNumberOfPages();
				for (int i = 0; i < nPages; i++) {
					if (!sharedData.getStopFlag().isSet()) {
						stripper.setStartPage(i);
						stripper.setEndPage(i);
						String chunk = stripper.getText(document);
						Future<Void> f = sharedData.getExecTextAnalysis()
								.submit(new TextAnalysisTask(chunk, sharedData));
						logDebug("new chunk task allocated");
						futs.add(f);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logDebug(" done.");

		return futs;
	}

	protected void log(String msg) {
		System.out.println("[ Doc Loader Task ] " + msg);
	}

	protected void logDebug(String msg) {
		System.out.println("[ Doc Loader Task ] " + msg);
	}

}
