package pcd.ass02.es1;

/**
 * 
 * Assignment #02 - es 1
 * 
 * @author aricci
 *
 */
public class Main {
	public static void main(String[] args) {
		try {
			
			View view = new View();
			
			Controller controller = new Controller(view);
			view.addListener(controller);
			
			view.display();						
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
