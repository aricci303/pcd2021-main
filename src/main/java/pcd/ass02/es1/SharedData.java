package pcd.ass02.es1;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;

public class SharedData {

	private Flag stopFlag;
	private WordFreqMap map;
	private HashMap<String, String> wordsToDiscard;
	private ExecutorService execDocLoading;
	private ExecutorService execTextAnalysis;
	
	public SharedData(WordFreqMap map, HashMap<String, String> wordsToDiscard, Flag stopFlag, ExecutorService execDocLoading, ExecutorService execTextAnalysis) {
		this.map = map;
		this.wordsToDiscard = wordsToDiscard;
		this.stopFlag = stopFlag;
		this.execDocLoading = execDocLoading;
		this.execTextAnalysis = execTextAnalysis;
	}

	public HashMap<String, String> getWordsToDiscard(){
		return wordsToDiscard;
		
	}
	public WordFreqMap getMap() {
		return map;
	}
	
	public Flag getStopFlag() {
		return stopFlag;
	}
	
	public ExecutorService getExecDocLoading() {
		return this.execDocLoading;
	}

	public ExecutorService getExecTextAnalysis() {
		return this.execTextAnalysis;
	}
}
