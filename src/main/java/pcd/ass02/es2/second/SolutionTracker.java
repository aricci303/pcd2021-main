package pcd.ass02.es2.second;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import pcd.ass02.es2.first.Train;
import pcd.ass02.es2.first.TrainData;
import pcd.ass02.es2.first.TrainLib;
import pcd.ass02.es2.first.TravelSolution;

public class SolutionTracker extends AbstractVerticle {

	private TrainLib tl;
	private TravelSolution ts;
	private Controller controller;
	private long timerId;
	private boolean queryOngoing;
	private Flag stopped;
	
	public SolutionTracker(TravelSolution ts, Controller c, Flag stopped) {
		this.controller = c;
		this.ts = ts;
		this.stopped = stopped;
	}
	
	public void start() {
		log("started");
		queryOngoing = false;
		tl = new TrainLib(this.getVertx());
		timerId = this.getVertx().setPeriodic(1000, id -> {
			if (stopped.isSet()) {
				this.getVertx().cancelTimer(timerId);
			} else {
				if (!queryOngoing) {
					queryTrains();
				}
			}
		});		
		log("done");
	}

	private void queryTrains() {
		queryOngoing = true;
		List<Train> trains = ts.getTrains();
		List<Future> allFutures = new ArrayList<Future>();
		for (Train t: trains) {	
			Future<TrainData> res = tl.getRealTimeTrainData(t.getNumericId());
			allFutures.add(res);
		}
		
		CompositeFuture
			.all(allFutures)
			.onSuccess(cf -> {
				queryOngoing = false;
				List<TrainData> tdList = cf.result().list();
				controller.notifySolutionState(tdList);
			});
		
		
	}
	private void log(String msg) {
		System.out.println("" + Thread.currentThread() + " " + msg);
	}	
}
