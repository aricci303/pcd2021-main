package pcd.ass02.es2.second;

import java.util.List;

import io.vertx.core.Vertx;
import pcd.ass02.es2.first.TrainData;
import pcd.ass02.es2.first.TravelSolution;

/**
 * 
 * Passive controller part, designed as a monitor.
 * 
 * @author aricci
 *
 */
public class Controller {

	private Flag stopFlag;
	private TrainTrackingView view;
	private Vertx  vertx;

	private List<TravelSolution> solutions;
	private TravelSolution trackedSolution;
	private List<TrainData> solutionState;
	
	public Controller() {
		this.stopFlag = new Flag();
		vertx = Vertx.vertx();
	}
	
	public synchronized void setView(TrainTrackingView view) {
		this.view = view;
	}
	
	public synchronized void notifySolutions(List<TravelSolution> solutions) {
		this.solutions = solutions;
		view.updateSolutions(solutions);
	}
	
	public synchronized void notifySolutionToTrack(int index) {
		this.trackedSolution = solutions.get(index);
		this.stopFlag.reset();
		SolutionTracker tracker = new SolutionTracker(trackedSolution, this, stopFlag);
		vertx.deployVerticle(tracker);
		view.startTracking(trackedSolution);
	}

	public synchronized void notifySolutionState(List<TrainData> tdList) {
		solutionState = tdList;
		view.updateSolutionState(solutionState);
	}

	public synchronized void notifySolutionToFind(String from, String to, String date, String time) {
		view.resetSolutions();
		TrainSolutionFinder finder = new TrainSolutionFinder(from, to, date, time, this);
		vertx.deployVerticle(finder);
		
	}
		
	public synchronized void notifyStopTracking() {
		stopFlag.set();
		view.stopTracking();
	}

}
