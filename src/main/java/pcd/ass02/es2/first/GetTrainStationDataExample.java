package pcd.ass02.es2.first;

import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class GetTrainStationDataExample {

	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		TrainLib tl = new TrainLib(vertx);
		
		Future<TrainStationData> res = tl.getRealTrainStationData("S05066Cesena");
		res.onSuccess(s -> {
			dump("-- PARTENZE --");
			for (Departure dep: s.getDepartures()) {
				dump(dep.getDest() + " - ore: " + dep.getTime() + " - treno: " + dep.getTrainId());
				
			}
			dump("-- ARRIVI --");
			for (Arrival arr: s.getArrivals()) {
				dump(arr.getDest() + " - ore: " + arr.getTime() + " - treno: " + arr.getTrainId());
				
			}
		});
	}
	
	private static void dump(String st) {
		System.out.println(st);
	}
	
}
