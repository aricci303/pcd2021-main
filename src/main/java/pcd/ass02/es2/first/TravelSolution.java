package pcd.ass02.es2.first;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TravelSolution {

	private LocalDateTime depTime;
	private String depStation;
	
	private LocalDateTime arrTime;
	private String arrStation;
	
	private List<Train> trains;

	public TravelSolution(LocalDateTime depTime, String depStation, LocalDateTime arrTime, String arrStation) {
		super();
		this.depTime = depTime;
		this.depStation = depStation;
		this.arrTime = arrTime;
		this.arrStation = arrStation;
		trains = new ArrayList<Train>();
	}
	
	public LocalDateTime getDepTime() {
		return depTime;
	}

	public String getDepStation() {
		return depStation;
	}

	public LocalDateTime getArrTime() {
		return arrTime;
	}

	public String getArrStation() {
		return arrStation;
	}

	public List<Train> getTrains() {
		return trains;
	}

	public void addTrain(Train t) {
		trains.add(t);
	}
	
}
