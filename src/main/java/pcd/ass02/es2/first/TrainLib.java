package pcd.ass02.es2.first;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import scala.concurrent.Promise;

public class TrainLib {

	private Vertx vertx;
	private final DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    private CloseableHttpClient httpClient;
    
	public TrainLib(Vertx vertx) {
		this.vertx = vertx;
        httpClient = HttpClients.createDefault();
	}
	
	
	public Future<List<TravelSolution>> getTrainSolutions(String origin, String dest, String date, String time) {
		Future<List<TravelSolution>> fut = vertx.executeBlocking(promise -> {			
			List<TravelSolution> solutions = new ArrayList<TravelSolution>();
			try {
				HttpGet request = new HttpGet("https://www.lefrecce.it/msite/api/solutions?origin="+origin+"&destination="+dest+"&arflag=A&adate="+date+"&atime="+time+"&adultno=1&childno=0&direction=A&frecce=false&onlyRegional=false");
			    CloseableHttpResponse response = httpClient.execute(request);
			        
			    if (response.getStatusLine().getStatusCode() == 200) {
					String content = EntityUtils.toString(response.getEntity());
			        JsonArray res = new JsonArray(content); 
					for (Object o: res) {
						JsonObject sol = (JsonObject) o;
						long depTimeTs = sol.getLong("departuretime");
						LocalDateTime depTime =
						        LocalDateTime.ofInstant(Instant.ofEpochMilli(depTimeTs), 
						                                TimeZone.getDefault().toZoneId());  

						long arrTimeTs = sol.getLong("arrivaltime");
						LocalDateTime arrTime =
						        LocalDateTime.ofInstant(Instant.ofEpochMilli(arrTimeTs), 
						                                TimeZone.getDefault().toZoneId());  

						
						String or = sol.getString("origin");
						String de = sol.getString("destination");
						String idsol = sol.getString("idsolution");
						TravelSolution ts = new TravelSolution(depTime, or, arrTime, de);
						solutions.add(ts);
						
						try {
							HttpGet request2 = new HttpGet("https://www.lefrecce.it/msite/api/solutions/"+idsol+"/details");
						    CloseableHttpResponse response2 = httpClient.execute(request2);
						    // response2.close();
						    if (response2.getStatusLine().getStatusCode() == 200) {
								String content2 = EntityUtils.toString(response2.getEntity());
	
								JsonArray res2 = null;
								try {
									res2 = new JsonArray(content2); 
								} catch (Exception ex) {
									System.out.println(content2);
									ex.printStackTrace();
								}
								if (res2 != null) {
									for (Object o2: res2) {
										JsonObject train = (JsonObject) o2;
										String id = train.getString("trainidentifier");
										Train tr = new Train(id);
										ts.addTrain(tr);
										JsonArray sl = train.getJsonArray("stoplist");
										for (Object st: sl) {
											JsonObject stop = (JsonObject) st;
											String stat = stop.getString("stationname");
											String arrivalTime = stop.getString("arrivaltime");
											Optional<LocalDateTime> arrDate = Optional.empty();
											if (arrivalTime != null) {
												arrDate = Optional.of(LocalDateTime.parse(arrivalTime, formatter));
											}
													
											String departTime = stop.getString("departuretime");
											
											Optional<LocalDateTime> depDate =  Optional.empty();
											if (departTime != null) {
												depDate = Optional.of(LocalDateTime.parse(departTime, formatter));
											}
			
											TrainStop trainStop = new TrainStop(stat, arrDate, depDate);
											tr.addTrainStop(trainStop);
										}
									}
								}
							}					
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					} 
					promise.complete(solutions);				
				}
			    // response.close();
			} catch (Exception ex) {
				Promise.failed(ex);
			}
		});
		return fut;
	}
	
	public Future<TrainData> getRealTimeTrainData(String trainId) {
		Future<TrainData> fut = vertx.executeBlocking(promise -> {			
			try {
				   Document doc = Jsoup.connect("http://www.viaggiatreno.it/vt_pax_internet/mobile/scheda?numeroTreno="+trainId).get();
				   Elements els =  doc.body().children();
				   /*
				   for (int i = 0; i < els.size(); i++) {
					   System.out.print(i + " ---> \n" + els.get(i) + "\n");
				   }
				   */				  
				   String tId = els.get(1).text();
				   
				   TrainData ti = new TrainData(tId, LocalTime.now());
				   TrainStopData origin = this.parseTrainStopData(els.get(2));
				   ti.setOrigin(origin);

				   if (origin.isValid()) {
					   if (els.size() <= 7) {
						   // already arrived;
						   TrainStopData dest = this.parseTrainStopData(els.get(4));
						   ti.setDestination(dest);
						   try {
							   if (ti.isAlreadyDeparted()) {
								   int nMins = this.parseDelayWhenArrived(els.get(5));
								   ti.setLastDetectionInfo("", null, nMins);
							   }
						   } catch (Exception ex) {
							   ex.printStackTrace();
						   }
					   } else {
						   // still ongoing
						   TrainStopData dest = this.parseTrainStopData(els.get(6));
						   ti.setDestination(dest);
						   TrainStopData last = this.parseTrainStopData(els.get(4));
						   int nMins = this.parseDelayWhenStillGoing(els.get(7));
						   ti.setLastStop(last);
						   
						   // last det
						   Element els2 = els.get(7).children().get(0);
						   // Il treno viaggia con 2 minuti di ritardo Ultimo rilevamento a MODENA alle ore 00:06
						   String[] toks = els2.text().split(" ");
						   try {
							   LocalTime detectionTime = LocalTime.parse(toks[toks.length - 1]);
							   String stat = toks[toks.length - 4];
							   ti.setLastDetectionInfo(stat, detectionTime, nMins);
						   } catch (Exception ex) {
							   ti.setLastDetectionInfo("", null, nMins);
						   }
						   
					   }
				   } else {
					   // train departure not happened yet, no info
					   ti.setDestination(new TrainStopData("", null, null, false));
				   }
					// System.out.println("==> " + station + " - " + plannedTime + " - " + actualTime);
				   promise.complete(ti);	
				} catch (Exception ex) {
					ex.printStackTrace();
					Promise.failed(ex);
				}
		});
		return fut;		
		
	}
	
	public Future<TrainStationData> getRealTrainStationData(String trainStationId) {
		Future<TrainStationData> fut = vertx.executeBlocking(promise -> {			
			try {
				   Document doc = 
						   Jsoup.connect("http://www.viaggiatreno.it/vt_pax_internet/mobile/stazione?codiceStazione="+trainStationId)
						   .userAgent("Mozilla")
						   .post();

				   TrainStationData td = new TrainStationData(trainStationId, LocalTime.now());
				   
				   Elements els =  doc.body().children();
				   int startIndexPartenze = 3;
				   int startIndexArrivi = 4;
				   for (int i = 4; i < els.size(); i++) {
					   if (els.get(i).toString().indexOf("<strong>Arrivi</strong>") > -1){
						   startIndexArrivi = i;
						   break;
					   }
				   }

				   // departures
				   
				   for (int i = startIndexPartenze; i < startIndexArrivi; i++) {
					   Elements par = els.get(i).getAllElements(); // partenze
					   String train = par.get(1).text();
					   String dest = par.get(3).text();
					   String time = par.get(5).text();
					   td.addDeparture(new Departure(train, dest, LocalTime.parse(time)));
				   }

				   // arrivals
				   
				   for (int i = startIndexArrivi + 1; i < els.size() - 1; i++) {
					   Elements arr = els.get(i).getAllElements(); // arrivi
					   String train = arr.get(1).text();
					   String from = arr.get(3).text();
					   String time = arr.get(5).text();
					   td.addArrival(new Arrival(train, from, LocalTime.parse(time)));
				   }

				   promise.complete(td);	
				} catch (Exception ex) {
					ex.printStackTrace();
					Promise.failed(ex);
				}
		});
		return fut;		
		
	}	
	
	private TrainStopData parseTrainStopData(Element el) {
		   Elements els = el.getAllElements();
		   try {
			   String station = els.get(1).getAllElements().get(0).text();
			   String plannedTime = els.get(4).getAllElements().get(0).text();
			   boolean done = els.get(5).getAllElements().get(0).text().indexOf("eff") > -1;
			   String actualTime = els.get(7).getAllElements().get(0).text();
			   LocalTime pt = !plannedTime.equals("") ? LocalTime.parse(plannedTime) : null;
			   LocalTime at = !actualTime.equals("") ? LocalTime.parse(actualTime) : null;		
			   return new TrainStopData(station, pt, at, done);
		   } catch (Exception ex) {
			   // train not 
			   return new TrainStopData("", null, null, false);
		   }
	}
	
	private int parseDelayWhenArrived(Element el) {
		   Element els2 = el.children().get(0);
		   // esempio: "Il treno e' arrivato con 6 minuti di ritardo"
		   String tt = els2.text().substring(25);
		   String[] toks = tt.split(" ");
		   int mins = Integer.parseInt(toks[0]);
		   if (toks[3].equals("anticipo")) {
			   mins = -mins;
		   }
		   return mins;
	}
	
	private int parseDelayWhenStillGoing(Element el) {
		   Element els2 = el.children().get(0);
		   // esempio: "Il treno viaggia con 2 minuti di ritardo Ultimo rilevamento a MODENA alle ore 00:06"
		   String tt = els2.text().substring(21);
		   String[] toks = tt.split(" ");
		   int mins = Integer.parseInt(toks[0]);
		   if (toks[2].equals("anticipo")) {
			   mins = -mins;
		   }
		   return mins;
	}
	
}
